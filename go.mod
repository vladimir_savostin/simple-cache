module bitbucket.org/vladimir_savostin/simple-cache

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v2 v2.2.8
)
