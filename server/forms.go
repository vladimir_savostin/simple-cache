package main

//JSONError - common error resp
type JSONError struct {
	Error string `json:"error"`
}

//SetReq - common form for SET requests for all data structures
//some fields will be unused for some data structs
type SetReq struct {
	Value string `json:"value"`
	Field string `json:"field"` //only for hashes
}

//SetReq - common form for GET requests for all data structures
type GetResp struct {
	Value string `json:"value"`
}

//SetReq - common form for GET keys requests for all data structures
//some fields will be unused for some data structs
type GetKeysReq struct {
	Key     string `json:"key"` //only for hashes
	Pattern string `json:"pattern"`
}

//GetKeysResp - common form for GET keys requests for all data structures
type GetKeysResp struct {
	Keys string `json:"keys"`
}
