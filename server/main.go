package main

import (
	"bitbucket.org/vladimir_savostin/simple-cache/cache"
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

const (
	paramKey     = "key"
	paramValue   = "value"
	paramField   = "field"
	paramTTL     = "ttl" //send as a query param, example /some/endpoint/?ttl=60s
	paramIndex   = "index"
	paramPattern = "pattern" //pattern for keys search
)

var Cache *cache.Cache
var Conf settings

func init() {

	data, err := ioutil.ReadFile("cache.yaml")
	if err != nil {
		log.Error(err)
		os.Exit(2)
	}

	log.SetLevel(log.DebugLevel)
	log.SetReportCaller(true)

	err = yaml.Unmarshal(data, &Conf)
	if err != nil {
		log.Error(err)
		os.Exit(2)
	}

	log.Debugf("current settings: %+v", Conf)

	Cache = cache.NewCache(
		cache.WithDefaultTTL(Conf.DefaultTTL),
		cache.WithCleanupInterval(Conf.CleanupInterval))
}

type settings struct {
	Port            string        `yaml:"port"`
	DefaultTTL      time.Duration `yaml:"default_ttl"`
	CleanupInterval time.Duration `yaml:"cleanup_interval"`
}

func main() {
	router := gin.Default()

	kv := router.Group("kv")
	list := router.Group("list")
	hash := router.Group("hash")

	kvCtrl := KVController{}

	kv.POST("/:key", checkKey, checkValue, checkTTL, kvCtrl.Set)
	kv.GET("/:key", checkKey, kvCtrl.Get)
	kv.DELETE("/:key", checkKey, kvCtrl.Remove)
	kv.GET("/", checkPattern, kvCtrl.GetKeys)

	listCtrl := ListController{}

	list.POST("/:key", checkKey, checkValue, checkTTL, listCtrl.Set)
	list.GET("/:key", checkKey, listCtrl.Get)
	list.GET("/:key/:index", checkKey, checkIndex, listCtrl.GetIndex)
	list.DELETE("/:key", checkKey, listCtrl.Remove)
	list.GET("/", checkPattern, listCtrl.GetKeys)

	hashCtrl := HashController{}

	hash.Use(checkKey)
	hash.POST("/:key/:field", checkField, checkValue, checkTTL, hashCtrl.Set)
	hash.GET("all/:key", hashCtrl.Get)
	hash.GET("kv/:key/:field", checkField, hashCtrl.GetField)
	hash.DELETE("/:key", hashCtrl.Remove)
	hash.GET("fields/:key", checkPattern, hashCtrl.GetKeys)

	panic(router.Run(fmt.Sprintf(":%s", Conf.Port)))
}

func checkKey(ctx *gin.Context) {
	key := ctx.Param(paramKey)
	if len(key) == 0 {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, JSONError{Error: "key was not provided"})
		return
	}
	ctx.Set(paramKey, key)
	ctx.Next()
}

func checkField(ctx *gin.Context) {
	field := ctx.Param(paramField)
	if len(field) == 0 {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, JSONError{Error: "field was not provided"})
		return
	}
	ctx.Set(paramField, field)
	ctx.Next()
}

func checkIndex(ctx *gin.Context) {
	indexStr := ctx.Param(paramIndex)
	if len(indexStr) == 0 {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, JSONError{Error: "indexStr was not provided"})
		return
	}

	index, err := strconv.Atoi(indexStr)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, JSONError{Error: err.Error()})
		return
	}

	ctx.Set(paramIndex, index)
	ctx.Next()
}

func checkValue(ctx *gin.Context) {
	var req SetReq
	err := ctx.BindJSON(&req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, JSONError{Error: err.Error()})
		return
	}

	if len(req.Value) == 0 {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, JSONError{Error: "value was not provided"})
		return
	}

	ctx.Set(paramValue, req.Value)
	ctx.Next()
}

func checkPattern(ctx *gin.Context) {
	pattern := ctx.Query(paramPattern)

	ctx.Set(paramPattern, pattern)
	ctx.Next()
}

func checkTTL(ctx *gin.Context) {
	var ttl time.Duration = 0
	ttlStr := ctx.Query(paramTTL)

	if len(ttlStr) != 0 {
		var err error
		ttl, err = time.ParseDuration(ttlStr)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, JSONError{Error: err.Error()})
			return
		}
	}

	ctx.Set(paramTTL, ttl)
	ctx.Next()
}
