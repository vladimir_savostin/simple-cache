package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type KVController struct {
}

func (c KVController) Set(ctx *gin.Context) {
	key := ctx.GetString(paramKey)
	value := ctx.GetString(paramValue)
	ttl := ctx.GetDuration(paramTTL)

	Cache.Set(key, value, ttl)

	ctx.Status(http.StatusOK)
}

func (c KVController) Get(ctx *gin.Context) {
	key := ctx.GetString(paramKey)

	val, ok := Cache.Get(key)
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, GetResp{Value: val})
}

func (c KVController) Remove(ctx *gin.Context) {
	key := ctx.GetString(paramKey)

	err := Cache.Remove(key)
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	ctx.Status(http.StatusOK)
}

func (c KVController) GetKeys(ctx *gin.Context) {
	pattern := ctx.GetString(paramPattern)

	keys, err := Cache.Keys(pattern)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, JSONError{Error: err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, GetKeysResp{Keys: strings.Join(keys, ",")})
}
