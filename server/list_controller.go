package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type ListController struct {
}

func (c ListController) Set(ctx *gin.Context) {
	key := ctx.GetString(paramKey)
	value := ctx.GetString(paramValue)
	ttl := ctx.GetDuration(paramTTL)

	Cache.LSet(key, value, ttl)

	ctx.Status(http.StatusOK)
}

func (c ListController) Get(ctx *gin.Context) {
	key := ctx.GetString(paramKey)

	vals, ok := Cache.LGet(key)
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	valStr := strings.Join(vals, ",")

	ctx.JSON(http.StatusOK, GetResp{Value: valStr})
}

func (c ListController) GetIndex(ctx *gin.Context) {
	key := ctx.GetString(paramKey)
	index := ctx.GetInt(paramIndex)

	val, err := Cache.LGetIndex(key, index)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, JSONError{Error: err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, GetResp{Value: val})
}

func (c ListController) Remove(ctx *gin.Context) {
	key := ctx.GetString(paramKey)

	err := Cache.LRemove(key)
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	ctx.Status(http.StatusOK)
}

func (c ListController) GetKeys(ctx *gin.Context) {
	pattern := ctx.GetString(paramPattern)

	keys, err := Cache.LKeys(pattern)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, JSONError{Error: err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, GetKeysResp{Keys: strings.Join(keys, ",")})
}
