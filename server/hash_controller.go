package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type HashController struct {
}

func (c HashController) Set(ctx *gin.Context) {
	key := ctx.GetString(paramKey)
	field := ctx.GetString(paramField)
	value := ctx.GetString(paramValue)
	ttl := ctx.GetDuration(paramTTL)

	Cache.HSet(key, field, value, ttl)

	ctx.Status(http.StatusOK)
}

func (c HashController) Get(ctx *gin.Context) {
	key := ctx.GetString(paramKey)

	data, ok := Cache.HGet(key)
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	//form comma separated field:value pairs
	b := strings.Builder{}
	for field, value := range data {
		b.WriteString(field)
		b.WriteString(":")
		b.WriteString(value)
		b.WriteString(",")
	}
	resp := b.String()
	resp = strings.TrimSuffix(resp, ",")

	ctx.JSON(http.StatusOK, GetResp{Value: resp})
}

func (c HashController) GetField(ctx *gin.Context) {
	key := ctx.GetString(paramKey)
	field := ctx.GetString(paramField)

	val, ok := Cache.HGetField(key, field)
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, GetResp{Value: val})
}

func (c HashController) Remove(ctx *gin.Context) {
	key := ctx.GetString(paramKey)

	err := Cache.HRemove(key)
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	ctx.Status(http.StatusOK)
}

func (c HashController) GetKeys(ctx *gin.Context) {
	key := ctx.GetString(paramKey)
	pattern := ctx.GetString(paramPattern)

	keys, err := Cache.HKeys(key, pattern)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, JSONError{Error: err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, GetKeysResp{Keys: strings.Join(keys, ",")})
}
