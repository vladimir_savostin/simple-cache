# README #



### What is this repository for? ###

This is a simple implementation of redis cache analog for testing purposes

### Subsystems ###
This repository include 3 modules:

*  [cache](cache) - core cache implementation
*  [server](server) - http server with able to run cache core. See [API specification](server/swagger.yaml)
*  [client](client) - api client library for server

### Inputs ###
#### cache.yaml ####
configuration file for cache server, include:

* `port` - port to listen
* `default_ttl` - default expiration time for entries
* `cleanup_interval` - intervals between collecting and removing old entries

### Usage example ###

First of all we need to setup and run cache server. Lets create configuration file

`nano server/cache.yaml`:

```yaml
port: 8082
default_ttl: 5m
cleanup_interval: 10s
```

This config says that we will listen on port `8082`, with default ttl=5 minutes, and 10 seconds cleanup intarval

Run the cahce server:

```bash
cd server
go run main.go
```

If all is ok, you will see setup logs

```
[GIN-debug] Listening and serving HTTP on :8082
```

Then run the simple api-client example:

```bash
cd ../client/example
go run main.go
```

You should see output like this:
```
val=val1 ok=true err=<nil>
keys=[key1 key2] err=<nil>
keys=[key2] err=<nil>
```