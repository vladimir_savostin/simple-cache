package cache

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestCacheString(t *testing.T) {
	data := map[string]string{
		"key1": "val1",
		"key2": "val2",
	}

	cache := NewCache()
	for key, val := range data {
		cache.Set(key, val, 0)
	}

	for key, val := range data {
		v, ok := cache.Get(key)
		assert.Equal(t, true, ok)
		assert.Equal(t, val, v)
	}

	countMatchedKeys := func(keys []string) int {
		matchedKeysCount := 0
		for _, k := range keys {
			for key := range data {
				if k == key {
					matchedKeysCount++
				}
			}
		}
		return matchedKeysCount
	}

	keys, err := cache.Keys("key\\d")
	assert.Nil(t, err)
	assert.Equal(t, 2, countMatchedKeys(keys))

	keys, err = cache.Keys("^\\d")
	assert.Nil(t, err)
	assert.Equal(t, 0, countMatchedKeys(keys))

	keys, err = cache.Keys("key1")
	assert.Nil(t, err)
	assert.Equal(t, 1, countMatchedKeys(keys))

	for key, _ := range data {
		err = cache.Remove(key)
		assert.Nil(t, err)
		v, ok := cache.Get(key)
		assert.Equal(t, false, ok)
		assert.Equal(t, "", v)
	}

	keys, err = cache.Keys("key\\d")
	assert.Nil(t, err)
	assert.Equal(t, 0, countMatchedKeys(keys))

}

func TestCacheStringTTL(t *testing.T) {
	const ttl = time.Millisecond * 200

	cache := NewCache()
	cache.Set("key", "val", ttl)

	v, ok := cache.Get("key")
	assert.Equal(t, true, ok)
	assert.Equal(t, "val", v)

	time.Sleep(ttl)

	v, ok = cache.Get("key")
	assert.Equal(t, false, ok)
	assert.Equal(t, "", v)
}
