package cache

import (
	"errors"
	"sync"
	"time"
)

const (
	defaultTTL             = time.Minute
	defaultCleanupInterval = time.Second * 10

	logKey       = "key"
	logValue     = "val"
	logField     = "field"
	logIndex     = "index"
	logTTL       = "ttl"
	logOperation = "op"
	logType      = "type"
	logPattern   = "pattern"
)

var (
	ErrNotFound   = errors.New("entry not found")
	ErrOutOfRange = errors.New("index out of range")
)

type Cache struct {
	//stringStorage - store all key-value pairs as simple key-value redis analog
	stringStorage *stringStore

	//listStorage - store all key-list pairs
	//it behaves as a string storage, but lets to store a list of values for the same key
	//and to get value from list by its index
	listStorage *listStore

	//hashStorage - store all key-hashmap pairs
	hashStorage *hashStore

	defaultTTL      time.Duration
	cleanupInterval time.Duration
}

//stringStore - represents a string key-value storage
type stringStore struct {
	mu   *sync.RWMutex
	data map[string]stringItem
}

//stringItem - represents a string key-value item
type stringItem struct {
	item
	value string
}

//listStore - represents a string key-value storage
type listStore struct {
	mu   *sync.RWMutex
	data map[string]listItem
}

//listItem - represents a key-list item
type listItem struct {
	item
	values []string
}

//hashStore - represents a string key-hash map storage
type hashStore struct {
	mu   *sync.RWMutex
	data map[string]hashItem
}

//hashItem - represents a key-hash map item
type hashItem struct {
	item
	values map[string]string
}

//item - represents the common item fields
type item struct {
	expiration int64
	created    time.Time
}

type CacheOption func(c *Cache)

func WithDefaultTTL(t time.Duration) CacheOption {
	return func(c *Cache) {
		c.defaultTTL = t
	}
}

func WithCleanupInterval(t time.Duration) CacheOption {
	return func(c *Cache) {
		c.cleanupInterval = t
	}
}

func NewCache(opts ...CacheOption) *Cache {
	cache := &Cache{
		stringStorage: &stringStore{
			mu:   &sync.RWMutex{},
			data: make(map[string]stringItem),
		},
		listStorage: &listStore{
			mu:   &sync.RWMutex{},
			data: make(map[string]listItem),
		},
		hashStorage: &hashStore{
			mu:   &sync.RWMutex{},
			data: make(map[string]hashItem),
		},
		defaultTTL:      defaultTTL,
		cleanupInterval: defaultCleanupInterval,
	}

	for _, opt := range opts {
		opt(cache)
	}

	//todo start GC
	return cache
}

// StartGC - start Garbage Collection
func (c *Cache) StartGC() {
	go c.GC()
}

// GC - Garbage Collection
func (c *Cache) GC() {

	for range time.After(c.cleanupInterval) {
		if c.stringStorage == nil {
			return
		}

		if keys := c.expiredKeys(); len(keys) != 0 {
			//c.clearItems(keys)
		}

	}

}

// expiredKeys - returns key list which are expired.
func (c *Cache) expiredKeys() (keys []string) {

	c.stringStorage.mu.RLock()

	defer c.stringStorage.mu.RUnlock()

	for k, i := range c.stringStorage.data {
		if time.Now().UnixNano() > i.expiration && i.expiration > 0 {
			keys = append(keys, k)
		}
	}

	return
}
