package cache

import (
	log "github.com/sirupsen/logrus"
	"regexp"
	"time"
)

//LSet - add new value to values list associated with provided key
//new value will be added from the right
func (c *Cache) LSet(key string, value string, ttl time.Duration) {
	log.WithFields(log.Fields{
		logOperation: "set", logType: "list", logKey: key, logValue: value, logTTL: ttl}).Debug()

	var expiration int64

	if ttl <= 0 {
		ttl = c.defaultTTL
	}
	expiration = time.Now().Add(ttl).UnixNano()

	oldValues, _ := c.LGet(key)
	oldValues = append(oldValues, value)

	c.listStorage.mu.Lock()
	defer c.listStorage.mu.Unlock()

	var newItem listItem
	newItem.values = oldValues
	newItem.expiration = expiration
	newItem.created = time.Now()
	c.listStorage.data[key] = newItem

}

//LGet - returns all values associated with a key
func (c *Cache) LGet(key string) ([]string, bool) {
	log.WithFields(log.Fields{
		logOperation: "get", logType: "list", logKey: key}).Debug()

	c.listStorage.mu.RLock()

	item, found := c.listStorage.data[key]
	if !found {
		//we should to unlock explicitly here and below to avoid deadlocks in Remove call below
		c.listStorage.mu.RUnlock()
		return []string{}, false
	}

	c.listStorage.mu.RUnlock()

	if time.Now().UnixNano() > item.expiration {
		err := c.LRemove(key)
		if err != nil {
			log.WithFields(log.Fields{
				logKey:       key,
				logOperation: "remove"}).Errorf("remove expired key: %s", err)
		}
		return []string{}, false
	}

	//todo is there need to update expiration time??

	return item.values, true
}

//LGetIndex - returns value at the index position from the list associated with a key
func (c *Cache) LGetIndex(key string, index int) (string, error) {
	log.WithFields(log.Fields{
		logOperation: "get_index", logType: "list", logKey: key, logIndex: index}).Debug()

	list, ok := c.LGet(key)
	if !ok {
		return "", ErrNotFound
	}
	if index > len(list) || index < 0 {
		return "", ErrOutOfRange
	}
	return list[index], nil
}

func (c *Cache) LRemove(key string) error {
	log.WithFields(log.Fields{
		logOperation: "remove", logType: "list", logKey: key}).Debug()

	c.listStorage.mu.Lock()
	defer c.listStorage.mu.Unlock()

	if _, found := c.listStorage.data[key]; !found {
		return ErrNotFound
	}

	delete(c.listStorage.data, key)

	return nil
}

//Keys - returns list of the keys, which match a provided pattern
func (c *Cache) LKeys(pattern string) (keys []string, err error) {
	log.WithFields(log.Fields{
		logOperation: "keys", logType: "list", logPattern: pattern}).Debug()

	c.listStorage.mu.Lock()
	defer c.listStorage.mu.Unlock()

	r, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}

	for key, item := range c.listStorage.data {
		if r.MatchString(key) && time.Now().UnixNano() < item.expiration {
			keys = append(keys, key)
		}
	}

	return keys, nil
}
