package cache

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"regexp"
	"time"
)

func (c *Cache) Set(key string, value string, ttl time.Duration) {
	log.WithFields(log.Fields{
		logOperation: "set", logType: "kv", logKey: key, logValue: value, logTTL: ttl}).Debug()
	var expiration int64

	if ttl <= 0 {
		ttl = c.defaultTTL
	}
	expiration = time.Now().Add(ttl).UnixNano()

	c.stringStorage.mu.Lock()
	defer c.stringStorage.mu.Unlock()

	var newItem stringItem
	newItem.value = value
	newItem.expiration = expiration
	newItem.created = time.Now()
	fmt.Printf("%+v\n", c.stringStorage)
	c.stringStorage.data[key] = newItem
	fmt.Printf("%+v\n", c.stringStorage)

}

func (c *Cache) Get(key string) (string, bool) {
	log.WithFields(log.Fields{
		logOperation: "get", logType: "kv", logKey: key}).Debug()

	c.stringStorage.mu.RLock()

	item, found := c.stringStorage.data[key]
	if !found {
		//we should to unlock explicitly here and below to avoid deadlocks in Remove call below
		c.stringStorage.mu.RUnlock()
		return "", false
	}

	c.stringStorage.mu.RUnlock()

	if time.Now().UnixNano() > item.expiration {
		err := c.Remove(key)
		if err != nil {
			log.WithFields(log.Fields{
				logKey:       key,
				logOperation: "remove"}).Errorf("remove expired key: %s", err)
		}
		return "", false
	}

	//todo is there need to update expiration time??

	return item.value, true
}

func (c *Cache) Remove(key string) error {
	log.WithFields(log.Fields{
		logOperation: "remove", logType: "kv", logKey: key}).Debug()

	c.stringStorage.mu.Lock()
	defer c.stringStorage.mu.Unlock()

	if _, found := c.stringStorage.data[key]; !found {
		return ErrNotFound
	}

	delete(c.stringStorage.data, key)

	return nil
}

//Keys - returns list of the keys, which match a provided pattern
func (c *Cache) Keys(pattern string) (keys []string, err error) {
	log.WithFields(log.Fields{
		logOperation: "keys", logType: "kv", logPattern: pattern}).Debug()

	c.stringStorage.mu.Lock()
	defer c.stringStorage.mu.Unlock()

	r, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}

	for key, item := range c.stringStorage.data {
		if r.MatchString(key) && time.Now().UnixNano() < item.expiration {
			keys = append(keys, key)
		}
	}

	return keys, nil
}
