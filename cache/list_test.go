package cache

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestCacheList(t *testing.T) {
	data := map[string][]string{
		"key1": {"val11", "val12", "val13"},
		"key2": {"val2"},
	}

	cache := NewCache()
	for key, vals := range data {
		for _, val := range vals {
			cache.LSet(key, val, 0)
		}
	}

	for key, vals := range data {
		v, ok := cache.LGet(key)
		assert.Equal(t, true, ok)
		assert.Equal(t, vals, v)

		_, err := cache.LGetIndex(key, 1000)
		assert.Equal(t, ErrOutOfRange, err)

		for i, val := range vals {
			v, err := cache.LGetIndex(key, i)
			assert.Nil(t, err)
			assert.Equal(t, val, v)
		}
	}

	countMatchedKeys := func(keys []string) int {
		matchedKeysCount := 0
		for _, k := range keys {
			for key := range data {
				if k == key {
					matchedKeysCount++
				}
			}
		}
		return matchedKeysCount
	}

	keys, err := cache.LKeys("key\\d")
	assert.Nil(t, err)
	assert.Equal(t, 2, countMatchedKeys(keys))

	keys, err = cache.LKeys("^\\d")
	assert.Nil(t, err)
	assert.Equal(t, 0, countMatchedKeys(keys))

	keys, err = cache.LKeys("key1")
	assert.Nil(t, err)
	assert.Equal(t, 1, countMatchedKeys(keys))

	for key, _ := range data {
		err = cache.LRemove(key)
		assert.Nil(t, err)
		v, ok := cache.LGet(key)
		assert.Equal(t, false, ok)
		assert.Equal(t, []string{}, v)
	}

	keys, err = cache.LKeys("key\\d")
	assert.Nil(t, err)
	assert.Equal(t, 0, countMatchedKeys(keys))

}

func TestCacheListTTL(t *testing.T) {
	const ttl = time.Millisecond * 200

	cache := NewCache()
	cache.Set("key", "val", ttl)

	v, ok := cache.Get("key")
	assert.Equal(t, true, ok)
	assert.Equal(t, "val", v)

	time.Sleep(ttl)

	v, ok = cache.Get("key")
	assert.Equal(t, false, ok)
	assert.Equal(t, "", v)
}
