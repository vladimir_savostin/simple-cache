package cache

import (
	log "github.com/sirupsen/logrus"
	"regexp"
	"time"
)

//HSet - add new field-value pair associated with provided key
func (c *Cache) HSet(key, field, value string, ttl time.Duration) {
	log.WithFields(log.Fields{
		logOperation: "set", logType: "hash", logKey: key, logValue: value, logField: field, logTTL: ttl}).
		Debug()

	var expiration int64

	if ttl <= 0 {
		ttl = c.defaultTTL
	}
	expiration = time.Now().Add(ttl).UnixNano()

	oldValues, _ := c.HGet(key)
	if oldValues == nil {
		oldValues = make(map[string]string)
	}
	oldValues[field] = value

	c.hashStorage.mu.Lock()
	defer c.hashStorage.mu.Unlock()

	var newItem hashItem
	newItem.values = oldValues
	newItem.expiration = expiration
	newItem.created = time.Now()
	c.hashStorage.data[key] = newItem

}

//HGet - returns all field-value pairs associated with a key
func (c *Cache) HGet(key string) (map[string]string, bool) {
	log.WithFields(log.Fields{
		logOperation: "get", logType: "hash", logKey: key}).Debug()

	c.hashStorage.mu.RLock()

	item, found := c.hashStorage.data[key]
	if !found {
		//we should to unlock explicitly here and below to avoid deadlocks in Remove call below
		c.hashStorage.mu.RUnlock()
		return nil, false
	}

	c.hashStorage.mu.RUnlock()

	if time.Now().UnixNano() > item.expiration {
		err := c.HRemove(key)
		if err != nil {
			log.WithFields(log.Fields{
				logKey:       key,
				logOperation: "remove"}).Errorf("remove expired key: %s", err)
		}
		return nil, false
	}

	//todo is there need to update expiration time??

	return item.values, true
}

//HGetField - returns value associated with a key and its field
func (c *Cache) HGetField(key, field string) (string, bool) {
	log.WithFields(log.Fields{
		logOperation: "get_field", logType: "hash", logKey: key, logField: field}).Debug()

	data, ok := c.HGet(key)
	if !ok {
		return "", false
	}
	value, ok := data[field]

	return value, ok
}

func (c *Cache) HRemove(key string) error {
	log.WithFields(log.Fields{
		logOperation: "set", logType: "hash", logKey: key}).Debug()

	c.hashStorage.mu.Lock()
	defer c.hashStorage.mu.Unlock()

	if _, found := c.hashStorage.data[key]; !found {
		return ErrNotFound
	}

	delete(c.hashStorage.data, key)

	return nil
}

//Keys - returns list of the fields, associated with a given key, which match a provided pattern
func (c *Cache) HKeys(key, pattern string) (fields []string, err error) {
	log.WithFields(log.Fields{
		logOperation: "set", logType: "hash", logKey: key}).Debug()

	r, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}

	data, _ := c.HGet(key)
	if data == nil {
		return []string{}, ErrNotFound
	}

	c.hashStorage.mu.Lock()
	defer c.hashStorage.mu.Unlock()

	for field, _ := range data {
		if r.MatchString(field) {
			fields = append(fields, field)
		}
	}

	return fields, nil
}
