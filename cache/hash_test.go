package cache

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestCacheHash(t *testing.T) {
	data := map[string]map[string]string{
		"post": {
			"Moscow":       "12345",
			"SPB":          "11111",
			"Antananarivo": "22222",
		},
		"age": {
			"Vova": "10",
			"Ira":  "11",
			"Kira": "12",
		},
	}

	cache := NewCache()
	for key, vals := range data {
		for field, val := range vals {
			cache.HSet(key, field, val, 0)
		}
	}

	for key, hash := range data {
		v, ok := cache.HGet(key)
		assert.Equal(t, true, ok)
		assert.Equal(t, hash, v)

		for filed, val := range hash {
			v, ok := cache.HGetField(key, filed)
			assert.Equal(t, true, ok)
			assert.Equal(t, val, v)
		}
	}

	countMatchedKeys := func(keys []string) int {
		matchedKeysCount := 0
		for _, k := range keys {
			for field := range data["post"] {
				if k == field {
					matchedKeysCount++
				}
			}
		}
		return matchedKeysCount
	}

	keys, err := cache.HKeys("post", "^\\w{3,6}$")
	assert.Nil(t, err)
	assert.Equal(t, 2, countMatchedKeys(keys))

	keys, err = cache.HKeys("post", "^\\d")
	assert.Nil(t, err)
	assert.Equal(t, 0, countMatchedKeys(keys))

	keys, err = cache.HKeys("post", "Moscow")
	assert.Nil(t, err)
	assert.Equal(t, 1, countMatchedKeys(keys))

	for key, _ := range data {
		err = cache.HRemove(key)
		assert.Nil(t, err)
		_, ok := cache.HGet(key)
		assert.Equal(t, false, ok)

	}

}

func TestCacheHashTTL(t *testing.T) {
	const ttl = time.Millisecond * 200

	cache := NewCache()
	cache.HSet("key", "field", "val", ttl)

	v, ok := cache.HGet("key")
	assert.Equal(t, true, ok)
	assert.Equal(t, map[string]string{"field": "val"}, v)

	time.Sleep(ttl)

	v, ok = cache.HGet("key")
	assert.Equal(t, false, ok)
}
