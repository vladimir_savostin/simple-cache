package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

//CacheClient - simple implementation of the Cacher interface
type CacheClient struct {
	client  *http.Client
	baseUrl string
}

func NewCacher(addr string) Cacher {
	c := &CacheClient{}
	c.client = &http.Client{
		Transport: &http.Transport{
			MaxIdleConns:        10,
			MaxIdleConnsPerHost: 10,
			MaxConnsPerHost:     50,
		},
		Timeout: time.Second * 10,
	}
	c.baseUrl = addr
	return c
}

func (c *CacheClient) Set(key string, value string, ttl time.Duration) error {
	reqData := SetReq{
		Value: value,
	}
	data, err := json.Marshal(reqData)
	if err != nil {
		return err
	}
	req, err := http.NewRequest(
		"POST",
		fmt.Sprintf("http://%s/kv/%s?ttl=%s", c.baseUrl, key, ttl),
		bytes.NewReader(data))
	if err != nil {
		return err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	return nil
}

func (c *CacheClient) Get(key string) (string, bool, error) {
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("http://%s/kv/%s", c.baseUrl, key),
		nil)
	if err != nil {
		return "", false, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return "", false, err
	}

	if resp.StatusCode != http.StatusOK {
		return "", false, fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var respData GetResp
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return "", false, err
	}

	return respData.Value, true, nil
}

func (c *CacheClient) Remove(key string) error {
	req, err := http.NewRequest(
		"DELETE",
		fmt.Sprintf("http://%s/kv/%s?ttl=%s", c.baseUrl, key),
		nil)
	if err != nil {
		return err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	return nil
}

func (c *CacheClient) Keys(pattern string) (keys []string, err error) {
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("http://%s/kv?pattern=%s", c.baseUrl, pattern),
		nil)
	if err != nil {
		return []string{}, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return []string{}, err
	}

	if resp.StatusCode != http.StatusOK {
		return []string{}, fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var respData GetKeysResp
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return []string{}, err
	}

	data := strings.Split(respData.Keys, ",")
	return data, nil
}

func (c *CacheClient) LSet(key string, value string, ttl time.Duration) error {
	reqData := SetReq{
		Value: value,
	}
	data, err := json.Marshal(reqData)
	if err != nil {
		return err
	}
	req, err := http.NewRequest(
		"POST",
		fmt.Sprintf("http://%s/list/%s?ttl=%s", c.baseUrl, key, ttl),
		bytes.NewReader(data))
	if err != nil {
		return err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	return nil
}

func (c *CacheClient) LGet(key string) ([]string, bool, error) {
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("http://%s/list/%s", c.baseUrl, key),
		nil)
	if err != nil {
		return []string{}, false, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return []string{}, false, err
	}

	if resp.StatusCode != http.StatusOK {
		return []string{}, false, fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var respData GetResp
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return []string{}, false, err
	}

	data := strings.Split(respData.Value, ",")

	return data, true, nil
}

func (c *CacheClient) LGetIndex(key string, index int) (string, error) {
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("http://%s/list/%s/%v", c.baseUrl, key, index),
		nil)
	if err != nil {
		return "", err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var respData GetResp
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return "", err
	}

	return respData.Value, nil
}

func (c *CacheClient) LRemove(key string) error {
	req, err := http.NewRequest(
		"DELETE",
		fmt.Sprintf("http://%s/kv/%s", c.baseUrl, key),
		nil)
	if err != nil {
		return err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	return nil
}

func (c *CacheClient) LKeys(pattern string) (keys []string, err error) {
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("http://%s/list?pattern=%s", c.baseUrl, pattern),
		nil)
	if err != nil {
		return []string{}, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return []string{}, err
	}

	if resp.StatusCode != http.StatusOK {
		return []string{}, fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var respData GetKeysResp
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return []string{}, err
	}

	data := strings.Split(respData.Keys, ",")
	return data, nil
}

func (c *CacheClient) HSet(key, field, value string, ttl time.Duration) error {
	reqData := SetReq{
		Value: value,
	}
	data, err := json.Marshal(reqData)
	if err != nil {
		return err
	}
	req, err := http.NewRequest(
		"POST",
		fmt.Sprintf("http://%s/hash/%s/%s?ttl=%s", c.baseUrl, key, field, ttl),
		bytes.NewReader(data))
	if err != nil {
		return err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	return nil
}

func (c *CacheClient) HGet(key string) (map[string]string, bool, error) {
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("http://%s/hash/all/%s", c.baseUrl, key),
		nil)
	if err != nil {
		return nil, false, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, false, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, false, fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var respData GetResp
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return nil, false, err
	}

	data := make(map[string]string)

	pairsData := strings.Split(respData.Value, ",")
	for _, pair := range pairsData {
		pairData := strings.Split(pair, ":")
		if len(pairData) < 2 {
			return nil, false, fmt.Errorf("invalid filed:value pair format")
		}
		data[pairData[0]] = pairData[1]
	}

	return data, true, nil
}

func (c *CacheClient) HGetField(key, field string) (string, bool, error) {
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("http://%s/hash/kv/%s/%s", c.baseUrl, key, field),
		nil)
	if err != nil {
		return "", false, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return "", false, err
	}

	if resp.StatusCode != http.StatusOK {
		return "", false, fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var respData GetResp
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return "", false, err
	}

	return respData.Value, true, nil
}

func (c *CacheClient) HRemove(key string) error {
	req, err := http.NewRequest(
		"DELETE",
		fmt.Sprintf("http://%s/hash/%s", c.baseUrl, key),
		nil)
	if err != nil {
		return err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	return nil
}

func (c *CacheClient) HKeys(key, pattern string) (fields []string, err error) {
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("http://%s/hash/fields/%s?pattern=%s", c.baseUrl, key, pattern),
		nil)
	if err != nil {
		return []string{}, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return []string{}, err
	}

	if resp.StatusCode != http.StatusOK {
		return []string{}, fmt.Errorf("got wrong http status code: %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var respData GetKeysResp
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return []string{}, err
	}

	data := strings.Split(respData.Keys, ",")
	return data, nil
}
