package client

import (
	"time"
)

type Cacher interface {
	//Set - set new value to provided key
	//new value will override the old one
	Set(key string, value string, ttl time.Duration) error

	//Get - returns values associated with a key from kv struct
	Get(key string) (string, bool, error)

	//Remove - remove key and its values from kv struct
	Remove(key string) error

	//Keys - returns list of the keys, which match a provided pattern
	Keys(pattern string) (keys []string, err error)

	//LSet - add new value to values list associated with provided key
	//new value will be added from the right
	LSet(key string, value string, ttl time.Duration) error

	//LGet - returns all values (list) associated with a key
	LGet(key string) ([]string, bool, error)

	//LGetIndex - returns value at the index position from the list associated with a key
	LGetIndex(key string, index int) (string, error)

	//LRemove -  remove key and its values from list struct
	LRemove(key string) error

	//Keys - returns list of the keys, which match a provided pattern
	LKeys(pattern string) (keys []string, err error)

	//HSet - add new field-value pair associated with provided key
	HSet(key, field, value string, ttl time.Duration) error

	//HGet - returns all field-value pairs associated with a key
	HGet(key string) (map[string]string, bool, error)

	//HGetField - returns value associated with a key and its field
	HGetField(key, field string) (string, bool, error)

	//HRemove - remove key and its values from hash struct
	HRemove(key string) error

	//Keys - returns list of the fields, associated with a given key, which match a provided pattern
	HKeys(key, pattern string) (fields []string, err error)
}
