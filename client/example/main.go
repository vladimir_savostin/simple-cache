package main

import (
	cacher "bitbucket.org/vladimir_savostin/simple-cache/client"
	"fmt"
	"os"
	"time"
)

func main() {
	client := cacher.NewCacher("127.0.0.1:8082")
	err := client.Set("key1", "val1", time.Minute)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	err = client.Set("key2", "val2", time.Minute)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	val, ok, err := client.Get("key1")
	fmt.Printf("val=%s ok=%v err=%v\n", val, ok, err) //will print "val=val1 ok=true err=nil"

	keys, err := client.Keys("key\\d")
	fmt.Printf("keys=%s err=%v\n", keys, err) //will print "keys=[key1 key2] err=nil"

	client.Remove("key1")

	keys, err = client.Keys("key\\d")
	fmt.Printf("keys=%s err=%v\n", keys, err) //will print "keys=[key2] err=nil"

}
